package si.uni_lj.fri.pbd.miniapp1
import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.MediaStore
import android.util.Base64
import android.view.MenuItem
import android.view.View
import android.widget.ImageButton
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.cursoradapter.widget.SimpleCursorAdapter
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_contacts.*
import java.io.ByteArrayOutputStream


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    var cols = listOf<String>(
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.CommonDataKinds.Phone._ID
    ).toTypedArray()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)


        //navigaton drawer code
        val toolbar = findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar);
        val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout);
        val toggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        toggle.isDrawerIndicatorEnabled = true;
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState();
        navigaton.setNavigationItemSelectedListener(this)

        //ask uuser fo perimission to read contacts
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, Array(1) { Manifest.permission.READ_CONTACTS }, 111)
        } else {
            readConctacts()
        }

        //switch to home fragment
        switchFragmentAndTitle(HomeFragment(), getString(R.string.home_title));
        //load lastly taken profile picture
        loadPicture()
    }


    //if user previously denied permission to read contacts ask again
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 111 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            readConctacts();
    }


    //switch between fragment according to button clicked HOME, CONTACTS OR MESSAGE
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        //when you click on HOME,CONTACTS OR MESSAGE it hides the drawer
        drawer_layout.closeDrawer(GravityCompat.START);
        when (item.itemId) {
            R.id.home -> {
                switchFragmentAndTitle(HomeFragment(), getString(R.string.home_title)); }
            R.id.contacts -> {
                switchFragmentAndTitle(ContactsFragment(), getString(R.string.contacts_title)); }
            R.id.message -> {
                switchFragmentAndTitle(MessageFragment(), getString(R.string.message_title)); }
        }
        return true;
    }

    //switches between fragments and changes title
    fun switchFragmentAndTitle(f: Fragment, title: String) {
        supportActionBar?.title = title;
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, f).commit();
    }


    //user clicked on profile picture
    fun takePicture(view: View) {
        pictureIntent()
    }

    val REQUEST_IMAGE_CAPTURE = 1

    //sent intent for picture
    private fun pictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
        } catch (e: ActivityNotFoundException) {
            error(getString(R.string.camera_error))
        }
    }


    //get picture from camera and store it in sharedpreferences
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap;
            //make picture bigger


            val stringPicture = BitToString(imageBitmap)
            val sharedPreferences: SharedPreferences = getSharedPreferences(getString(R.string.sharedPref), Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString(getString(R.string.STRING_KEY), stringPicture).apply();
            //set image
            loadPicture()
        }
    }

    //loads picture from sharedpreferences and set it
    fun loadPicture() {
        val sharedPreferences: SharedPreferences = getSharedPreferences(getString(R.string.sharedPref), Context.MODE_PRIVATE)
        val sPicture = sharedPreferences.getString(getString(R.string.STRING_KEY), null)
        if(sPicture != null){
            val pic = StringToBi(sPicture);
            val full = Bitmap.createScaledBitmap(pic!!, 390, 385, false) as Bitmap
            val header: View = navigaton.getHeaderView(0)
            val imageBut = header.findViewById<ImageButton>(R.id.imageButton);
            imageBut.setImageBitmap(full)
            imageBut.invalidate()
        }
    }


    //reads contacts from phone
    private fun readConctacts() {

        var rs = contentResolver?.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, cols, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        var from = listOf<String>(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER).toTypedArray()
        var to = intArrayOf(android.R.id.text1, android.R.id.text2);
        var adapter = SimpleCursorAdapter(this, android.R.layout.simple_expandable_list_item_2, rs, from, to, 0)
        list_view?.adapter = adapter
    }


    //converts bitmap image to string
    fun BitToString(bitmap: Bitmap): String? {
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos)
        val b = baos.toByteArray()
        return Base64.encodeToString(b, Base64.DEFAULT)
    }


    //string to bitmap
    fun StringToBi(encodedString: String?): Bitmap? {
        val encodeByte = Base64.decode(encodedString, Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)
    }

}