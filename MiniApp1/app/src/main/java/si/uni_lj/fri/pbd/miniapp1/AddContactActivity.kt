package si.uni_lj.fri.pbd.miniapp1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.Button
import android.widget.EditText

class AddContactActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_contact)

        val name = findViewById<EditText>(R.id.etName)
        val email = findViewById<EditText>(R.id.etEmail)
        val phone = findViewById<EditText>(R.id.etPhone)
        val addContact = findViewById<Button>(R.id.btnAdd)


        //adds new contact to phone
        addContact.setOnClickListener{
            if(name.text.toString().isNotEmpty() && email.text.toString().isNotEmpty() && phone.text.toString().isNotEmpty()){
                val intent = Intent(ContactsContract.Intents.Insert.ACTION).apply {
                    // Sets the MIME type to match the Contacts Provider
                    type = ContactsContract.RawContacts.CONTENT_TYPE

                    //add name
                    putExtra(ContactsContract.Intents.Insert.NAME, name?.text.toString() )
                    //add email address
                    putExtra(ContactsContract.Intents.Insert.EMAIL, email?.text)
                    putExtra(
                            ContactsContract.Intents.Insert.EMAIL_TYPE,
                            ContactsContract.CommonDataKinds.Email.TYPE_HOME
                    )
                    //add phone number
                    putExtra(ContactsContract.Intents.Insert.PHONE, phone?.text)
                    putExtra(
                            ContactsContract.Intents.Insert.PHONE_TYPE,
                            ContactsContract.CommonDataKinds.Phone.TYPE_HOME
                    )
                }

                startActivityForResult(intent,1);

            }
            //error for ensuring the user filled required fields
            if(name.text.toString().isEmpty()) name.setError(getString(R.string.user_error))
            if(email.text.toString().isEmpty()) email.setError(getString(R.string.user_error))
            if(phone.text.toString().isEmpty()) phone.setError(getString(R.string.user_error))
        }
    }

    //when contact is added returns to homescreen
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val backHome = Intent();
        backHome.setClass(this!!,MainActivity::class.java)
        startActivity(backHome);
    }


}