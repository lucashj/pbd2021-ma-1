package si.uni_lj.fri.pbd.miniapp1


import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cursoradapter.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_contacts.*


class ContactsFragment : Fragment(),View.OnClickListener {

    var cols = listOf<String>(
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.CommonDataKinds.Phone._ID
    ).toTypedArray()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        readConctacts()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        readConctacts()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v =  inflater.inflate(R.layout.fragment_contacts, container, false)
        val button = v.findViewById<FloatingActionButton>(R.id.fab)
        button.setOnClickListener(this);
        return v
    }


    //reads contacts from phone
    private fun readConctacts() {
        var cRes = activity?.contentResolver;
        var rs = cRes?.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, cols, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);

        var from = listOf<String>(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER).toTypedArray()

        var to = intArrayOf(android.R.id.text1, android.R.id.text2);

        var adapter = SimpleCursorAdapter(activity, android.R.layout.simple_expandable_list_item_2, rs, from, to, 0)

        list_view?.adapter = adapter
    }


    //switches to addcontactactivity
    override fun onClick(v: View?) {
        val intent = Intent();
        intent.setClass(activity!!,AddContactActivity::class.java)
        startActivity(intent)
    }


}